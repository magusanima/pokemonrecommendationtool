function addtoform()
{
    // Count and increment total number of inputs
    // In pure javascript use 'dataset' to select 'data-index' due to hyphen issues
    var index = document.getElementById("form_id").dataset.total;
    index++;
    document.getElementById("form_id").dataset.total = index;
    // Make container variable
    let html = "";
    // Grab html content of form_template
    html += document.getElementById("form_template").innerHTML
    // Remove opening and closing comment tags
    html = html.replace(/(<!--)/g, "");
    html = html.replace(/(-->)/g, "");
    // Replace "replace_me" with index number
    html = html.replace(/(replace_me)/g, index);
    // Output variable using form_id - insertAdjacent needed to avoid clearing of originally inputted values on click
    document.getElementById("form_id").insertAdjacentHTML("beforeend", html);
}

function first_type_change(this_el)
{
    // Grab type 2 options list
    let index = this_el.dataset.index;
    let type2_select = document.getElementById("type2_" + index);

    // Make sure all options are enabled
    for (var i = 0; i < type2_select.options.length; i++)
    {
        type2_select.options[i].disabled = false;
    }

    // Disable selected option in second type option list
    type2_select.options[this_el.selectedIndex].disabled = "disabled";
}

function second_type_change(this_el)
{
    // Change first option in type array to 'None' after a type is changed
    this_el.options[0].innerHTML = "None";
}
