<?php
// Open 'you' Database
$servername = "fizzy-mysql";
$username = "root";
$password = "password";
$dbname = "fizzy";

$conn = new mysqli($servername, $username, $password, $dbname);
if ($conn->connect_error)
{
    die("Connection failed: " . $conn->connect_error);
}

$selected_pokemon_html = "";

// Output chosen Pokémon and Type
if ($_SERVER["REQUEST_METHOD"] == "POST")
{
    $pokemon_input = [];

    for ($n = 1; $n <= 7; $n++)
    {
        $sql = "SELECT type FROM you
                WHERE id = {$_POST['type1'][$n]}";
        $result = $conn->query($sql);
        if($result)
        {
            $selected_type1 = $result->fetch_assoc();
        }

        $sql = "SELECT type FROM you
                WHERE id = {$_POST['type2'][$n]}";
        $result = $conn->query($sql);
        // Reset $selected_type2 to empty value between loop iterations
        $selected_type2 = [];
        if($result)
        {
            $selected_type2 = $result->fetch_assoc();
        }

        $input_name = test_input($_POST["name"][$n]);
        if (isset($_POST['type1'][$n])) {
            // Put inputted Pokémon and types into an array
            $pokemon_input[] = array($input_name, $_POST['type1'][$n], $_POST['type2'][$n]);

            // Output selected Pokemon and types
            $selected_pokemon_html .= $input_name . "  ";
            $selected_pokemon_html .= $selected_type1['type'];
            if ($selected_type2['type'])
                $selected_pokemon_html .= ' / ' . $selected_type2['type'];
            $selected_pokemon_html .= "<br>";
        }
    }
}
?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="stylesheet.css">
    <title>Team Recommendation Tool</title>
</head>

<body>
<div>
    <h1>Team Recommendation Tool</h1>
    <p>Use this form to add your favourites and estimate the team combination with the widest type coverage.</p>
    <br>
</div>
<div class="clear_fix">
    <!--  Vary width of this div depending on whether the "confirm_input" div has been generated/processed yet.  -->
    <div id="form_input" <?php echo ($selected_pokemon_html?"":"class='full_width'"); ?>>
        <form action="#" method="post">
            <?php

            $sql = "SELECT id, type FROM you";
            $result = $conn->query($sql);

            // Put Query results into a new array
            $you = [];
            if($result)
            {
                while($row = $result->fetch_assoc())
                {
                    $you[] = $row;
                }
            }

            // Create 7 form fields for input
            for($n=1; $n<=7; $n++)
            {
                echo '<label>Pokemon Name:
            <input type="text"'. $n .' id="name"'. $n .' name="name['. $n .']" value="'.$_POST['name'][$n].'" size="10">
            </label>';

                if(empty($_POST['name'][$n]))
                {
                    $name_error_message="Please add a species name.";
                    echo $name_error_message;
                    echo '<br>';
                }

                echo '<br>';

    // Pull type data directly from 'you' database
                for($i=1; $i<=2; $i++)
                {
                    echo '<label>' . ($i == 2 ? "Secondary " : "") . 'Type:
                <select name="type' . $i . '['. $n .']" id="type' . $i . '">
                <option value="" ' . ($i == 1 ? 'disabled' : "") . ($i == 1 && !$_POST["type" . $i][$n] ? ' selected' : "") . '>Select type..</option>
                ';
                    foreach($you as $y)
                    {
                        echo "<option value='{$y["id"]}' ". ($y["id"] == $_POST["type" . $i][$n] ? "selected" : "") .">{$y["type"]}</option>";
                    }
                    echo '
            </select>
        </label>
        ';

    // todo: (EXTRA) add javascript to remove already selected option from type2 select form
    // todo: (EXTRA) add javascript to change 'Select type...' to None after dropdown has been clicked (and back again if nothing is chosen).
                }

                if(!isset($_POST['type1'][$n]))
                {
                    $type_error_message="Please select at least one type.";
                    echo $type_error_message;
                }

                echo '<br><br>';

    // todo: (EXTRA) add javascript to allow adding of additional Pokemon

            }
            ?>
            <input type='submit' name='submit' value='Add'>
            <br><br><br>
        </form>
    </div>

    <div id="confirm_input">
        <?php
        echo $selected_pokemon_html;
        ?>
    </div>
</div>
<?php

// Validate Input
function test_input($input_name)
{
    $input_name = trim($input_name);
    $input_name = stripslashes($input_name);
    $input_name = htmlspecialchars($input_name);
    return $input_name;
}

// Make unique groups of 6 Pokemon each
function unique_teams($map, $size, &$generated = [], $loop = 1, $i = 0, $prefix = [])
{
    if($loop == 1)
    {
        sort($map);
    }
    for(; $i < count($map); $i++)
    {
        if($loop < $size)
        {
            unique_teams($map, $size, $generated, $loop + 1, $i + 1, [...$prefix, $map[$i]]);
        }
        else
        {
            $generated[] = [...$prefix, $map[$i]];
        }
    }
    return $generated;
}

// Calculate individual vs. effectiveness score and add this to the $pokemon_input array

// Function to lookup tables to find effectiveness values
function get_vseffectiveness($you_id, $conn)
{
    $sql = "SELECT competitor_id, effectiveness
            FROM lookup
            WHERE you_id='$you_id'";
    return $conn->query($sql);
}

// Add to pokemon_input array
foreach($pokemon_input as $index => $pokemon)
{
    $type1 = $pokemon[1];
    $result = get_vseffectiveness($type1, $conn);
    if($result)
    {
        while($row = $result->fetch_assoc())
        {
            $pokemon_input[$index]["vs. effectiveness"][$row["competitor_id"]] = $row["effectiveness"];
        }
    }

// If there is a secondary type, add these scores to vs. effectiveness array
    $type2 = $pokemon[2];
    if($type2)
    {
        $result = get_vseffectiveness($type2, $conn);
        if($result)
        {
            while($row = $result->fetch_assoc())
            {
                // if both vs.effectiveness scores are the same, do not add together
                if($pokemon_input[$index]["vs. effectiveness"][$row["competitor_id"]] != $row["effectiveness"])
                {
                    $pokemon_input[$index]["vs. effectiveness"][$row["competitor_id"]] += $row["effectiveness"];
                }
            }
        }
    }
}

// Run function to create unique teams of 6
$processed_unique_teams = unique_teams($pokemon_input, 6);

// Calculate vs. effectiveness scores for each team
    // Loop through pokemon in each team, and add up vs. effectiveness scores for the team totals, associate this with the $processed_unique_teams team
    // add individual pokemon's vs.effectiveness scores together into one new team vs.effectiveness array in $processed_unique_teams
foreach($processed_unique_teams as $index2 => $listedteam)
{
    foreach($listedteam as $index4 => $pokemon_in_team)
    {
        foreach($pokemon_in_team["vs. effectiveness"] as $competitor_id => $score)
        {
            $processed_unique_teams[$index2]["team vs. effectiveness"][$competitor_id] += $score;
        }
        unset($processed_unique_teams[$index2][$index4]["vs. effectiveness"]);
    }
}

// Remove teams with scores of 0 or less in any team vs.effectiveness
foreach($processed_unique_teams as $index3 => $listedteam)
{
    // Calculate total 'general' team score
    $processed_unique_teams[$index3]["teamscore"] = array_sum($listedteam["team vs. effectiveness"]);

    foreach($listedteam["team vs. effectiveness"] as $score)
    {
        if($score <= 0)
        {
            unset($processed_unique_teams[$index3]);
            break;
        }
    }
}

// Sort array by team general score
usort($processed_unique_teams, function ($a, $b)
    {
        if($a["teamscore"] == $b["teamscore"])
        {
            return 0;
        }
        elseif($a["teamscore"] < $b["teamscore"])
        {
            return 1;
        }
        return -1;
    }
);

// Return a max of 4 teams
$processed_unique_teams = array_slice($processed_unique_teams, 0, 4);

// Output $processed_unique_teams into an array for display to user
?>

<!-- Recommended Teams Output -->
<div class="clear_fix">
    <!-- Output names, general team score and team effectiveness scores: "vs. Normal : 3" etc. -->
<?php
    if($processed_unique_teams)
        {
            echo "<h2>Recommended Team(s)</h2>";
        }

    foreach($processed_unique_teams as $teamscore => $team1)
    {
?>
    <div class="clear_fix">
        <div id="team_output">
            <div id="team_score">
                <p>Team Score =
                    <?php
                    echo $team1['teamscore'];
                    ?>
                </p>
            </div>

            <div>
                <?php
                for($tp = 0; $tp <= 5; $tp++)
                {
                    // Output Pokémon in the team
                    echo "<div id='team_pokemon'>{$team1[$tp][0]}</div>";
                }
                ?>
            </div>

            <div id="team_vs_effectiveness">
                <p>Team Vs. Effectiveness</p>
                <?php
                    for($typ = 1; $typ <= 18; $typ++)
                    {
                        // Output vs. Type
                        $sql = "SELECT type FROM you
                        WHERE id = '$typ'";
                        $result = $conn->query($sql);
                        if ($result) {
                            $vstype = $result->fetch_assoc();
                        }
                        foreach($vstype as $row)
                            {
                                echo "<div>vs. {$row} = {$team1['team vs. effectiveness'][$typ]}</div>";
                            }
                    }
                ?>
            </div>
    </div>
</div>
<?php
    }
?>

    <div id="debug" class="clear_fix">
        <?php
        // DEBUGGING

        // All POSTed data
        //echo "<pre>";
        //print_r($_POST);
        //echo "</pre>";

        //pokemon_input array
        //echo "<pre>";
        //print_r($pokemon_input);
        //echo "</pre>";

        // Array of unique teams
        //echo "<pre>";
        //print_r(unique_teams($_POST["name"], 6));
        //echo "</pre>";

        // Array of unique teams with their types
        //echo "<pre>";
        //print_r($processed_unique_teams);
        //echo "</pre>";
        ?>
    </div>
</div>
</body>
</html>

<?php
// Close database connection
$conn->close();

?>