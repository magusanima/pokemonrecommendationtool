<?php

//print_r ($_POST);
$type_set = true;
$selected_type1 = $_POST['type1'];
$selected_type2 = $_POST['type2'];

// Error Messages - Missing Input
if(empty($_POST['name']))
{
    $name_error_message="Please add a species name.";
    echo $name_error_message;
}

if(!isset($_POST['type1']))
{
    $type_error_message="Please select at least one type.";
    $type_set = false;
    echo $type_error_message;
}

// Validate Input
function test_input($input_name)
{
    $input_name = trim($input_name);
    $input_name = stripslashes($input_name);
    $input_name = htmlspecialchars($input_name);
    return $input_name;
}

// Output chosen Pokémon and Type
if ($_SERVER["REQUEST_METHOD"] == "POST")
{
    $input_name = test_input($_POST["name"]);
    if ($type_set != false)
    {
        echo $input_name;
        echo $selected_type1;
        if($selected_type2)
            echo ' / '. $selected_type2;
    }
}