<?php
// Open 'you' Database
$servername = "fizzy-mysql";
$username = "root";
$password = "password";
$dbname = "fizzy";

$conn = new mysqli($servername, $username, $password, $dbname);
if ($conn->connect_error)
{
    die("Connection failed: " . $conn->connect_error);
}

// Function creating single block of 3 form inputs
function pokemonform_block($n, $you)
{
    echo '<label>Pokémon Name:
            <span>
                <input type="text" class="poke-form" id="name'. $n .'" name="name['. $n .']" value="'.htmlentities($_POST['name'][$n]).'" size="10">
                <img src="/images/pokeballicon.png" alt="pokeball">
            </span>
            </label>';

    if(empty($_POST['name'][$n]))
    {
        $name_error_message="Please add a species name.";
        echo $name_error_message;
        echo '<br>';
    }

    echo '<br>';

    // Pull type data directly from 'you' database
    for($i=1; $i<=2; $i++)
    {
        echo '<label>' . ($i == 2 ? "Secondary " : "") . 'Type:
                <select name="type' . $i . '['. $n .']" 
                    id="type' . $i . '_'. $n .'" 
                    data-index = "'. $n .'"
                    '. ($i == 2 ? "onchange='second_type_change(this)'" : "onchange='first_type_change(this)'") .'
                >
                <option value="" ' . ($i == 1 ? 'disabled' : "") . ($i == 1 && !$_POST["type" . $i][$n] ? ' selected' : "") . '>Select type..</option>
                ';
        foreach($you as $y)
        {
            echo "<option value='{$y["id"]}' ". ($y["id"] == $_POST["type" . $i][$n] ? "selected" : "") .">{$y["type"]}</option>";
        }
        echo '
            </select>
        </label>
        ';
    }

    if(!isset($_POST['type1'][$n]))
    {
        $type_error_message="Please select at least one type.";
        echo $type_error_message;
    }

    echo '<br><br>';
}


$selected_pokemon_html = "";

// Output chosen Pokémon and Type
if ($_SERVER["REQUEST_METHOD"] == "POST")
{
    $pokemon_input = [];

    foreach (array_keys($_POST["name"]) as $n)
    {
        $sql = "SELECT type FROM you
                WHERE id = {$_POST['type1'][$n]}";
        $result = $conn->query($sql);
        if($result)
        {
            $selected_type1 = $result->fetch_assoc();
        }

        $sql = "SELECT type FROM you
                WHERE id = {$_POST['type2'][$n]}";
        $result = $conn->query($sql);
        // Reset $selected_type2 to empty value between loop iterations
        $selected_type2 = [];
        if($result)
        {
            $selected_type2 = $result->fetch_assoc();
        }

        $input_name = test_input($_POST["name"][$n]);
        if (isset($_POST['type1'][$n])) {
            // Put inputted Pokémon and types into an array
            $pokemon_input[] = array($input_name, $_POST['type1'][$n], $_POST['type2'][$n]);

            // Output selected Pokemon and types
            $selected_pokemon_html .= $input_name . "   - ";
            $selected_pokemon_html .= $selected_type1['type'];
            if ($selected_type2['type'])
                $selected_pokemon_html .= ' / ' . $selected_type2['type'];
            $selected_pokemon_html .= "<br>";
        }
    }
}
?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="stylesheetflex.css">
    <title>Team Recommendation Tool</title>
    <script src="javascript.js"></script>
</head>

<body>
<div class="middle_body">
    <h1>Pokémon Team Recommendation Tool</h1>
    <p>Use this form to add your favourites and estimate the team combination with the widest type coverage.</p>
    <br>
</div>
<div class="flex-container middle_body">
    <!--  Vary width of this div depending on whether the "confirm_input" div has been generated/processed yet.  -->
    <div <?php echo ($selected_pokemon_html?"":"class='full_width'"); ?>>
        <form action="#" method="post">
            <div id="form_id" data-total="<?php echo ($_POST ? count($_POST["name"]) : "7"); ?>">
                <?php

                $sql = "SELECT id, type FROM you";
                $result = $conn->query($sql);

                // Put Query results into a new array
                $you = [];
                if($result)
                {
                    while($row = $result->fetch_assoc())
                    {
                        $you[] = $row;
                    }
                }

                // Saves user input by creating array using keys and values from name array
                // Prevents loss of javascript fields on submit
                $keys = [1, 2, 3, 4, 5, 6, 7];
                if($_POST)
                {
                    $keys = array_keys($_POST["name"]);
                }

                // Create 7 form fields for input
                foreach ($keys as $n)
                {
                    pokemonform_block($n, $you);
                }
                ?>
            </div>
            <button type="button" onclick="addtoform()">Add More Pokémon</button>
            <br><br>
            <input type='submit' name='submit' value='Submit'>
            <br><br>

        </form>
    </div>

    <div id="form_template">
        <!--
            <?php
                pokemonform_block("replace_me", $you);
            ?>
         -->
    </div>

    <div>
        <?php
        if($selected_pokemon_html)
        {
            echo "<div class=\"confirmed-list\">".$selected_pokemon_html."</div>";

//            STRING CONCATENATION - 4+ OPTIONS
//            echo '<div class="confirmed-list">'.$selected_pokemon_html.'</div>'; with '. concatenation
//
//            ---> echo "<div class=\"confirmed-list\">".$selected_pokemon_html."</div>"; Escaping \ and ". OR '. concatenation
//
//            echo "<div class=\"confirmed-list\">{$selected_pokemon_html}</div>"; {  } Curly braces and escaping \ - does not work with function calls
//
//            echo "<div class='confirmed-list'>{$selected_pokemon_html}</div>"; {  } Curly braces - does not work with function calls
        }
        ?>
    </div>
</div>
<?php

// Validate Input
function test_input($input_name)
{
    $input_name = trim($input_name);
    $input_name = stripslashes($input_name);
    $input_name = htmlspecialchars($input_name);
    return $input_name;
}

// Make unique groups of 6 Pokemon each
function unique_teams($map, $size, &$generated = [], $loop = 1, $i = 0, $prefix = [])
{
    if($loop == 1)
    {
        sort($map);
    }
    for(; $i < count($map); $i++)
    {
        if($loop < $size)
        {
            unique_teams($map, $size, $generated, $loop + 1, $i + 1, [...$prefix, $map[$i]]);
        }
        else
        {
            $generated[] = [...$prefix, $map[$i]];
        }
    }
    return $generated;
}

// Calculate individual vs. effectiveness score and add this to the $pokemon_input array

// Function to lookup tables to find effectiveness values
function get_vseffectiveness($you_id, $conn)
{
    $sql = "SELECT competitor_id, effectiveness
            FROM lookup
            WHERE you_id='$you_id'";
    return $conn->query($sql);
}

// Add to pokemon_input array
foreach($pokemon_input as $index => $pokemon)
{
    $type1 = $pokemon[1];
    $result = get_vseffectiveness($type1, $conn);
    if($result)
    {
        while($row = $result->fetch_assoc())
        {
            $pokemon_input[$index]["vs. effectiveness"][$row["competitor_id"]] = $row["effectiveness"];
        }
    }

// If there is a secondary type, add these scores to vs. effectiveness array
    $type2 = $pokemon[2];
    if($type2)
    {
        $result = get_vseffectiveness($type2, $conn);
        if($result)
        {
            while($row = $result->fetch_assoc())
            {
                // if both vs.effectiveness scores are the same, do not add together
                if($pokemon_input[$index]["vs. effectiveness"][$row["competitor_id"]] != $row["effectiveness"])
                {
                    $pokemon_input[$index]["vs. effectiveness"][$row["competitor_id"]] += $row["effectiveness"];
                }
            }
        }
    }
}

// Run function to create unique teams of 6
$processed_unique_teams = unique_teams($pokemon_input, 6);

// Calculate vs. effectiveness scores for each team
    // Loop through pokemon in each team, and add up vs. effectiveness scores for the team totals, associate this with the $processed_unique_teams team
    // add individual pokemon's vs.effectiveness scores together into one new team vs.effectiveness array in $processed_unique_teams
foreach($processed_unique_teams as $index2 => $listedteam)
{
    foreach($listedteam as $index4 => $pokemon_in_team)
    {
        foreach($pokemon_in_team["vs. effectiveness"] as $competitor_id => $score)
        {
            $processed_unique_teams[$index2]["team vs. effectiveness"][$competitor_id] += $score;
        }
        unset($processed_unique_teams[$index2][$index4]["vs. effectiveness"]);
    }
}

// Remove teams with scores of 0 or less in any team vs.effectiveness
foreach($processed_unique_teams as $index3 => $listedteam)
{
    // Calculate total 'general' team score
    $processed_unique_teams[$index3]["teamscore"] = array_sum($listedteam["team vs. effectiveness"]);

    foreach($listedteam["team vs. effectiveness"] as $score)
    {
        if($score <= 0)
        {
            unset($processed_unique_teams[$index3]);
            break;
        }
    }
}

// Sort array by team general score
usort($processed_unique_teams, function ($a, $b)
    {
        if($a["teamscore"] == $b["teamscore"])
        {
            return 0;
        }
        elseif($a["teamscore"] < $b["teamscore"])
        {
            return 1;
        }
        return -1;
    }
);

// Return a max of 4 teams
$processed_unique_teams = array_slice($processed_unique_teams, 0, 4);

// Output $processed_unique_teams into an array for display to user
?>

<hr>

<!-- Recommended Teams Output -->
    <!-- Output names, general team score and team effectiveness scores: "vs. Normal : 3" etc. -->
<div class="middle_body">
    <?php
    if($processed_unique_teams)
    {
        echo "<h2>Recommended Team(s)</h2>";
    }
    else
    {
        echo "<h2>The Pokémon selected do not have any viable combinations to reach an acceptable type coverage level.</h2>";
        echo "<h2>Please try again with a different selection.</h2>";
    }
    ?>
    <div class="teams">
        <?php
        foreach($processed_unique_teams as $teamscore => $team1)
        {
            ?>
            <div class="flex-container-team-output">
                <div class="AB">
                    <p class="bubble">Team Score =
                        <?php
                        echo $team1['teamscore'];
                        ?>
                    </p>

                    <?php
                    for($tp = 0; $tp <= 5; $tp++)
                    {
                        // Output Pokémon in the team
                        echo "<div class='bubble'>{$team1[$tp][0]}</div>";
                    }
                    ?>
                </div>

                <div class="C">
                    <p>Team Vs. Effectiveness</p>
                    <?php
                    for($typ = 1; $typ <= 18; $typ++)
                    {
                        // Output vs. Type
                        $sql = "SELECT type FROM you
                    WHERE id = '$typ'";
                        $result = $conn->query($sql);
                        if ($result) {
                            $vstype = $result->fetch_assoc();
                        }
                        foreach($vstype as $row)
                        {
                            echo "<div>vs. {$row} = {$team1['team vs. effectiveness'][$typ]}</div>";
                        }
                    }
                    ?>
                </div>
            </div>
            <?php
        }
        ?>
    </div>
</div>

<div id="debug">
    <?php
    // DEBUGGING
    // TODO: improve speed - very slow processing when 10+ pokemon inputted

    // All POSTed data
    //echo "<pre>";
    //print_r($_POST);
    //echo "</pre>";

    //pokemon_input array
    //echo "<pre>";
    //print_r($pokemon_input);
    //echo "</pre>";

    // Array of unique teams
    //echo "<pre>";
    //print_r(unique_teams($_POST["name"], 6));
    //echo "</pre>";

    // Array of unique teams with their types
    //echo "<pre>";
    //print_r($processed_unique_teams);
    //echo "</pre>";
    ?>
</div>

</body>
</html>

<?php
// Close database connection
$conn->close();

?>