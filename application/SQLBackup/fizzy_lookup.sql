create table lookup
(
    id            int auto_increment
        primary key,
    you_id        int null,
    competitor_id int null,
    effectiveness int null
);

INSERT INTO fizzy.lookup (id, you_id, competitor_id, effectiveness) VALUES (1, 1, 1, 1);
INSERT INTO fizzy.lookup (id, you_id, competitor_id, effectiveness) VALUES (2, 1, 2, 1);
INSERT INTO fizzy.lookup (id, you_id, competitor_id, effectiveness) VALUES (3, 1, 3, 1);
INSERT INTO fizzy.lookup (id, you_id, competitor_id, effectiveness) VALUES (4, 1, 4, 1);
INSERT INTO fizzy.lookup (id, you_id, competitor_id, effectiveness) VALUES (5, 1, 5, 1);
INSERT INTO fizzy.lookup (id, you_id, competitor_id, effectiveness) VALUES (6, 1, 6, -1);
INSERT INTO fizzy.lookup (id, you_id, competitor_id, effectiveness) VALUES (7, 1, 7, 1);
INSERT INTO fizzy.lookup (id, you_id, competitor_id, effectiveness) VALUES (8, 1, 8, -1);
INSERT INTO fizzy.lookup (id, you_id, competitor_id, effectiveness) VALUES (9, 1, 9, -1);
INSERT INTO fizzy.lookup (id, you_id, competitor_id, effectiveness) VALUES (10, 1, 10, 1);
INSERT INTO fizzy.lookup (id, you_id, competitor_id, effectiveness) VALUES (11, 1, 11, 1);
INSERT INTO fizzy.lookup (id, you_id, competitor_id, effectiveness) VALUES (12, 1, 12, 1);
INSERT INTO fizzy.lookup (id, you_id, competitor_id, effectiveness) VALUES (13, 1, 13, 1);
INSERT INTO fizzy.lookup (id, you_id, competitor_id, effectiveness) VALUES (14, 1, 14, 1);
INSERT INTO fizzy.lookup (id, you_id, competitor_id, effectiveness) VALUES (15, 1, 15, 1);
INSERT INTO fizzy.lookup (id, you_id, competitor_id, effectiveness) VALUES (16, 1, 16, 1);
INSERT INTO fizzy.lookup (id, you_id, competitor_id, effectiveness) VALUES (17, 1, 17, 1);
INSERT INTO fizzy.lookup (id, you_id, competitor_id, effectiveness) VALUES (18, 1, 18, 1);
INSERT INTO fizzy.lookup (id, you_id, competitor_id, effectiveness) VALUES (19, 2, 1, 2);
INSERT INTO fizzy.lookup (id, you_id, competitor_id, effectiveness) VALUES (20, 2, 2, 1);
INSERT INTO fizzy.lookup (id, you_id, competitor_id, effectiveness) VALUES (21, 2, 3, -1);
INSERT INTO fizzy.lookup (id, you_id, competitor_id, effectiveness) VALUES (22, 2, 4, -1);
INSERT INTO fizzy.lookup (id, you_id, competitor_id, effectiveness) VALUES (23, 2, 5, 1);
INSERT INTO fizzy.lookup (id, you_id, competitor_id, effectiveness) VALUES (24, 2, 6, 2);
INSERT INTO fizzy.lookup (id, you_id, competitor_id, effectiveness) VALUES (25, 2, 7, -1);
INSERT INTO fizzy.lookup (id, you_id, competitor_id, effectiveness) VALUES (26, 2, 8, -1);
INSERT INTO fizzy.lookup (id, you_id, competitor_id, effectiveness) VALUES (27, 2, 9, 2);
INSERT INTO fizzy.lookup (id, you_id, competitor_id, effectiveness) VALUES (28, 2, 10, 1);
INSERT INTO fizzy.lookup (id, you_id, competitor_id, effectiveness) VALUES (29, 2, 11, 1);
INSERT INTO fizzy.lookup (id, you_id, competitor_id, effectiveness) VALUES (30, 2, 12, 1);
INSERT INTO fizzy.lookup (id, you_id, competitor_id, effectiveness) VALUES (31, 2, 13, 1);
INSERT INTO fizzy.lookup (id, you_id, competitor_id, effectiveness) VALUES (32, 2, 14, -1);
INSERT INTO fizzy.lookup (id, you_id, competitor_id, effectiveness) VALUES (33, 2, 15, 2);
INSERT INTO fizzy.lookup (id, you_id, competitor_id, effectiveness) VALUES (34, 2, 16, 1);
INSERT INTO fizzy.lookup (id, you_id, competitor_id, effectiveness) VALUES (35, 2, 17, 2);
INSERT INTO fizzy.lookup (id, you_id, competitor_id, effectiveness) VALUES (36, 2, 18, -1);
INSERT INTO fizzy.lookup (id, you_id, competitor_id, effectiveness) VALUES (37, 3, 1, 1);
INSERT INTO fizzy.lookup (id, you_id, competitor_id, effectiveness) VALUES (38, 3, 2, 2);
INSERT INTO fizzy.lookup (id, you_id, competitor_id, effectiveness) VALUES (39, 3, 3, 1);
INSERT INTO fizzy.lookup (id, you_id, competitor_id, effectiveness) VALUES (40, 3, 4, 1);
INSERT INTO fizzy.lookup (id, you_id, competitor_id, effectiveness) VALUES (41, 3, 5, 1);
INSERT INTO fizzy.lookup (id, you_id, competitor_id, effectiveness) VALUES (42, 3, 6, -1);
INSERT INTO fizzy.lookup (id, you_id, competitor_id, effectiveness) VALUES (43, 3, 7, 2);
INSERT INTO fizzy.lookup (id, you_id, competitor_id, effectiveness) VALUES (44, 3, 8, 1);
INSERT INTO fizzy.lookup (id, you_id, competitor_id, effectiveness) VALUES (45, 3, 9, -1);
INSERT INTO fizzy.lookup (id, you_id, competitor_id, effectiveness) VALUES (46, 3, 10, 1);
INSERT INTO fizzy.lookup (id, you_id, competitor_id, effectiveness) VALUES (47, 3, 11, 1);
INSERT INTO fizzy.lookup (id, you_id, competitor_id, effectiveness) VALUES (48, 3, 12, 2);
INSERT INTO fizzy.lookup (id, you_id, competitor_id, effectiveness) VALUES (49, 3, 13, -1);
INSERT INTO fizzy.lookup (id, you_id, competitor_id, effectiveness) VALUES (50, 3, 14, 1);
INSERT INTO fizzy.lookup (id, you_id, competitor_id, effectiveness) VALUES (51, 3, 15, 1);
INSERT INTO fizzy.lookup (id, you_id, competitor_id, effectiveness) VALUES (52, 3, 16, 1);
INSERT INTO fizzy.lookup (id, you_id, competitor_id, effectiveness) VALUES (53, 3, 17, 1);
INSERT INTO fizzy.lookup (id, you_id, competitor_id, effectiveness) VALUES (54, 3, 18, 1);
INSERT INTO fizzy.lookup (id, you_id, competitor_id, effectiveness) VALUES (55, 4, 1, 1);
INSERT INTO fizzy.lookup (id, you_id, competitor_id, effectiveness) VALUES (56, 4, 2, 1);
INSERT INTO fizzy.lookup (id, you_id, competitor_id, effectiveness) VALUES (57, 4, 3, 1);
INSERT INTO fizzy.lookup (id, you_id, competitor_id, effectiveness) VALUES (58, 4, 4, -1);
INSERT INTO fizzy.lookup (id, you_id, competitor_id, effectiveness) VALUES (59, 4, 5, -1);
INSERT INTO fizzy.lookup (id, you_id, competitor_id, effectiveness) VALUES (60, 4, 6, -1);
INSERT INTO fizzy.lookup (id, you_id, competitor_id, effectiveness) VALUES (61, 4, 7, 1);
INSERT INTO fizzy.lookup (id, you_id, competitor_id, effectiveness) VALUES (62, 4, 8, -1);
INSERT INTO fizzy.lookup (id, you_id, competitor_id, effectiveness) VALUES (63, 4, 9, -1);
INSERT INTO fizzy.lookup (id, you_id, competitor_id, effectiveness) VALUES (64, 4, 10, 1);
INSERT INTO fizzy.lookup (id, you_id, competitor_id, effectiveness) VALUES (65, 4, 11, 1);
INSERT INTO fizzy.lookup (id, you_id, competitor_id, effectiveness) VALUES (66, 4, 12, 2);
INSERT INTO fizzy.lookup (id, you_id, competitor_id, effectiveness) VALUES (67, 4, 13, 1);
INSERT INTO fizzy.lookup (id, you_id, competitor_id, effectiveness) VALUES (68, 4, 14, 1);
INSERT INTO fizzy.lookup (id, you_id, competitor_id, effectiveness) VALUES (69, 4, 15, 1);
INSERT INTO fizzy.lookup (id, you_id, competitor_id, effectiveness) VALUES (70, 4, 16, 1);
INSERT INTO fizzy.lookup (id, you_id, competitor_id, effectiveness) VALUES (71, 4, 17, 1);
INSERT INTO fizzy.lookup (id, you_id, competitor_id, effectiveness) VALUES (72, 4, 18, 2);
INSERT INTO fizzy.lookup (id, you_id, competitor_id, effectiveness) VALUES (73, 5, 1, 1);
INSERT INTO fizzy.lookup (id, you_id, competitor_id, effectiveness) VALUES (74, 5, 2, 1);
INSERT INTO fizzy.lookup (id, you_id, competitor_id, effectiveness) VALUES (75, 5, 3, -1);
INSERT INTO fizzy.lookup (id, you_id, competitor_id, effectiveness) VALUES (76, 5, 4, 2);
INSERT INTO fizzy.lookup (id, you_id, competitor_id, effectiveness) VALUES (77, 5, 5, 1);
INSERT INTO fizzy.lookup (id, you_id, competitor_id, effectiveness) VALUES (78, 5, 6, 2);
INSERT INTO fizzy.lookup (id, you_id, competitor_id, effectiveness) VALUES (79, 5, 7, -1);
INSERT INTO fizzy.lookup (id, you_id, competitor_id, effectiveness) VALUES (80, 5, 8, 1);
INSERT INTO fizzy.lookup (id, you_id, competitor_id, effectiveness) VALUES (81, 5, 9, 2);
INSERT INTO fizzy.lookup (id, you_id, competitor_id, effectiveness) VALUES (82, 5, 10, 2);
INSERT INTO fizzy.lookup (id, you_id, competitor_id, effectiveness) VALUES (83, 5, 11, 1);
INSERT INTO fizzy.lookup (id, you_id, competitor_id, effectiveness) VALUES (84, 5, 12, -1);
INSERT INTO fizzy.lookup (id, you_id, competitor_id, effectiveness) VALUES (85, 5, 13, 2);
INSERT INTO fizzy.lookup (id, you_id, competitor_id, effectiveness) VALUES (86, 5, 14, 1);
INSERT INTO fizzy.lookup (id, you_id, competitor_id, effectiveness) VALUES (87, 5, 15, 1);
INSERT INTO fizzy.lookup (id, you_id, competitor_id, effectiveness) VALUES (88, 5, 16, 1);
INSERT INTO fizzy.lookup (id, you_id, competitor_id, effectiveness) VALUES (89, 5, 17, 1);
INSERT INTO fizzy.lookup (id, you_id, competitor_id, effectiveness) VALUES (90, 5, 18, 1);
INSERT INTO fizzy.lookup (id, you_id, competitor_id, effectiveness) VALUES (91, 6, 1, 1);
INSERT INTO fizzy.lookup (id, you_id, competitor_id, effectiveness) VALUES (92, 6, 2, -1);
INSERT INTO fizzy.lookup (id, you_id, competitor_id, effectiveness) VALUES (93, 6, 3, 2);
INSERT INTO fizzy.lookup (id, you_id, competitor_id, effectiveness) VALUES (94, 6, 4, 1);
INSERT INTO fizzy.lookup (id, you_id, competitor_id, effectiveness) VALUES (95, 6, 5, -1);
INSERT INTO fizzy.lookup (id, you_id, competitor_id, effectiveness) VALUES (96, 6, 6, 1);
INSERT INTO fizzy.lookup (id, you_id, competitor_id, effectiveness) VALUES (97, 6, 7, 2);
INSERT INTO fizzy.lookup (id, you_id, competitor_id, effectiveness) VALUES (98, 6, 8, 1);
INSERT INTO fizzy.lookup (id, you_id, competitor_id, effectiveness) VALUES (99, 6, 9, -1);
INSERT INTO fizzy.lookup (id, you_id, competitor_id, effectiveness) VALUES (100, 6, 10, 2);
INSERT INTO fizzy.lookup (id, you_id, competitor_id, effectiveness) VALUES (101, 6, 11, 1);
INSERT INTO fizzy.lookup (id, you_id, competitor_id, effectiveness) VALUES (102, 6, 12, 1);
INSERT INTO fizzy.lookup (id, you_id, competitor_id, effectiveness) VALUES (103, 6, 13, 1);
INSERT INTO fizzy.lookup (id, you_id, competitor_id, effectiveness) VALUES (104, 6, 14, 1);
INSERT INTO fizzy.lookup (id, you_id, competitor_id, effectiveness) VALUES (105, 6, 15, 2);
INSERT INTO fizzy.lookup (id, you_id, competitor_id, effectiveness) VALUES (106, 6, 16, 1);
INSERT INTO fizzy.lookup (id, you_id, competitor_id, effectiveness) VALUES (107, 6, 17, 1);
INSERT INTO fizzy.lookup (id, you_id, competitor_id, effectiveness) VALUES (108, 6, 18, 1);
INSERT INTO fizzy.lookup (id, you_id, competitor_id, effectiveness) VALUES (109, 7, 1, 1);
INSERT INTO fizzy.lookup (id, you_id, competitor_id, effectiveness) VALUES (110, 7, 2, -1);
INSERT INTO fizzy.lookup (id, you_id, competitor_id, effectiveness) VALUES (111, 7, 3, -1);
INSERT INTO fizzy.lookup (id, you_id, competitor_id, effectiveness) VALUES (112, 7, 4, -1);
INSERT INTO fizzy.lookup (id, you_id, competitor_id, effectiveness) VALUES (113, 7, 5, 1);
INSERT INTO fizzy.lookup (id, you_id, competitor_id, effectiveness) VALUES (114, 7, 6, 1);
INSERT INTO fizzy.lookup (id, you_id, competitor_id, effectiveness) VALUES (115, 7, 7, 1);
INSERT INTO fizzy.lookup (id, you_id, competitor_id, effectiveness) VALUES (116, 7, 8, -1);
INSERT INTO fizzy.lookup (id, you_id, competitor_id, effectiveness) VALUES (117, 7, 9, -1);
INSERT INTO fizzy.lookup (id, you_id, competitor_id, effectiveness) VALUES (118, 7, 10, -1);
INSERT INTO fizzy.lookup (id, you_id, competitor_id, effectiveness) VALUES (119, 7, 11, 1);
INSERT INTO fizzy.lookup (id, you_id, competitor_id, effectiveness) VALUES (120, 7, 12, 2);
INSERT INTO fizzy.lookup (id, you_id, competitor_id, effectiveness) VALUES (121, 7, 13, 1);
INSERT INTO fizzy.lookup (id, you_id, competitor_id, effectiveness) VALUES (122, 7, 14, 2);
INSERT INTO fizzy.lookup (id, you_id, competitor_id, effectiveness) VALUES (123, 7, 15, 1);
INSERT INTO fizzy.lookup (id, you_id, competitor_id, effectiveness) VALUES (124, 7, 16, 1);
INSERT INTO fizzy.lookup (id, you_id, competitor_id, effectiveness) VALUES (125, 7, 17, 2);
INSERT INTO fizzy.lookup (id, you_id, competitor_id, effectiveness) VALUES (126, 7, 18, -1);
INSERT INTO fizzy.lookup (id, you_id, competitor_id, effectiveness) VALUES (127, 8, 1, -1);
INSERT INTO fizzy.lookup (id, you_id, competitor_id, effectiveness) VALUES (128, 8, 2, 1);
INSERT INTO fizzy.lookup (id, you_id, competitor_id, effectiveness) VALUES (129, 8, 3, 1);
INSERT INTO fizzy.lookup (id, you_id, competitor_id, effectiveness) VALUES (130, 8, 4, 1);
INSERT INTO fizzy.lookup (id, you_id, competitor_id, effectiveness) VALUES (131, 8, 5, 1);
INSERT INTO fizzy.lookup (id, you_id, competitor_id, effectiveness) VALUES (132, 8, 6, 1);
INSERT INTO fizzy.lookup (id, you_id, competitor_id, effectiveness) VALUES (133, 8, 7, 1);
INSERT INTO fizzy.lookup (id, you_id, competitor_id, effectiveness) VALUES (134, 8, 8, 2);
INSERT INTO fizzy.lookup (id, you_id, competitor_id, effectiveness) VALUES (135, 8, 9, 1);
INSERT INTO fizzy.lookup (id, you_id, competitor_id, effectiveness) VALUES (136, 8, 10, 1);
INSERT INTO fizzy.lookup (id, you_id, competitor_id, effectiveness) VALUES (137, 8, 11, 1);
INSERT INTO fizzy.lookup (id, you_id, competitor_id, effectiveness) VALUES (138, 8, 12, 1);
INSERT INTO fizzy.lookup (id, you_id, competitor_id, effectiveness) VALUES (139, 8, 13, 1);
INSERT INTO fizzy.lookup (id, you_id, competitor_id, effectiveness) VALUES (140, 8, 14, 2);
INSERT INTO fizzy.lookup (id, you_id, competitor_id, effectiveness) VALUES (141, 8, 15, 1);
INSERT INTO fizzy.lookup (id, you_id, competitor_id, effectiveness) VALUES (142, 8, 16, 1);
INSERT INTO fizzy.lookup (id, you_id, competitor_id, effectiveness) VALUES (143, 8, 17, -1);
INSERT INTO fizzy.lookup (id, you_id, competitor_id, effectiveness) VALUES (144, 8, 18, 1);
INSERT INTO fizzy.lookup (id, you_id, competitor_id, effectiveness) VALUES (145, 9, 1, 1);
INSERT INTO fizzy.lookup (id, you_id, competitor_id, effectiveness) VALUES (146, 9, 2, 1);
INSERT INTO fizzy.lookup (id, you_id, competitor_id, effectiveness) VALUES (147, 9, 3, 1);
INSERT INTO fizzy.lookup (id, you_id, competitor_id, effectiveness) VALUES (148, 9, 4, 1);
INSERT INTO fizzy.lookup (id, you_id, competitor_id, effectiveness) VALUES (149, 9, 5, 1);
INSERT INTO fizzy.lookup (id, you_id, competitor_id, effectiveness) VALUES (150, 9, 6, 2);
INSERT INTO fizzy.lookup (id, you_id, competitor_id, effectiveness) VALUES (151, 9, 7, 1);
INSERT INTO fizzy.lookup (id, you_id, competitor_id, effectiveness) VALUES (152, 9, 8, 1);
INSERT INTO fizzy.lookup (id, you_id, competitor_id, effectiveness) VALUES (153, 9, 9, -1);
INSERT INTO fizzy.lookup (id, you_id, competitor_id, effectiveness) VALUES (154, 9, 10, -1);
INSERT INTO fizzy.lookup (id, you_id, competitor_id, effectiveness) VALUES (155, 9, 11, -1);
INSERT INTO fizzy.lookup (id, you_id, competitor_id, effectiveness) VALUES (156, 9, 12, 1);
INSERT INTO fizzy.lookup (id, you_id, competitor_id, effectiveness) VALUES (157, 9, 13, -1);
INSERT INTO fizzy.lookup (id, you_id, competitor_id, effectiveness) VALUES (158, 9, 14, 1);
INSERT INTO fizzy.lookup (id, you_id, competitor_id, effectiveness) VALUES (159, 9, 15, 2);
INSERT INTO fizzy.lookup (id, you_id, competitor_id, effectiveness) VALUES (160, 9, 16, 1);
INSERT INTO fizzy.lookup (id, you_id, competitor_id, effectiveness) VALUES (161, 9, 17, 1);
INSERT INTO fizzy.lookup (id, you_id, competitor_id, effectiveness) VALUES (162, 9, 18, 2);
INSERT INTO fizzy.lookup (id, you_id, competitor_id, effectiveness) VALUES (163, 10, 1, 1);
INSERT INTO fizzy.lookup (id, you_id, competitor_id, effectiveness) VALUES (164, 10, 2, 1);
INSERT INTO fizzy.lookup (id, you_id, competitor_id, effectiveness) VALUES (165, 10, 3, 1);
INSERT INTO fizzy.lookup (id, you_id, competitor_id, effectiveness) VALUES (166, 10, 4, 1);
INSERT INTO fizzy.lookup (id, you_id, competitor_id, effectiveness) VALUES (167, 10, 5, 1);
INSERT INTO fizzy.lookup (id, you_id, competitor_id, effectiveness) VALUES (168, 10, 6, -1);
INSERT INTO fizzy.lookup (id, you_id, competitor_id, effectiveness) VALUES (169, 10, 7, 2);
INSERT INTO fizzy.lookup (id, you_id, competitor_id, effectiveness) VALUES (170, 10, 8, 1);
INSERT INTO fizzy.lookup (id, you_id, competitor_id, effectiveness) VALUES (171, 10, 9, 2);
INSERT INTO fizzy.lookup (id, you_id, competitor_id, effectiveness) VALUES (172, 10, 10, -1);
INSERT INTO fizzy.lookup (id, you_id, competitor_id, effectiveness) VALUES (173, 10, 11, -1);
INSERT INTO fizzy.lookup (id, you_id, competitor_id, effectiveness) VALUES (174, 10, 12, 2);
INSERT INTO fizzy.lookup (id, you_id, competitor_id, effectiveness) VALUES (175, 10, 13, 1);
INSERT INTO fizzy.lookup (id, you_id, competitor_id, effectiveness) VALUES (176, 10, 14, 1);
INSERT INTO fizzy.lookup (id, you_id, competitor_id, effectiveness) VALUES (177, 10, 15, 2);
INSERT INTO fizzy.lookup (id, you_id, competitor_id, effectiveness) VALUES (178, 10, 16, -1);
INSERT INTO fizzy.lookup (id, you_id, competitor_id, effectiveness) VALUES (179, 10, 17, 1);
INSERT INTO fizzy.lookup (id, you_id, competitor_id, effectiveness) VALUES (180, 10, 18, 1);
INSERT INTO fizzy.lookup (id, you_id, competitor_id, effectiveness) VALUES (181, 11, 1, 1);
INSERT INTO fizzy.lookup (id, you_id, competitor_id, effectiveness) VALUES (182, 11, 2, 1);
INSERT INTO fizzy.lookup (id, you_id, competitor_id, effectiveness) VALUES (183, 11, 3, 1);
INSERT INTO fizzy.lookup (id, you_id, competitor_id, effectiveness) VALUES (184, 11, 4, 1);
INSERT INTO fizzy.lookup (id, you_id, competitor_id, effectiveness) VALUES (185, 11, 5, 2);
INSERT INTO fizzy.lookup (id, you_id, competitor_id, effectiveness) VALUES (186, 11, 6, 2);
INSERT INTO fizzy.lookup (id, you_id, competitor_id, effectiveness) VALUES (187, 11, 7, 1);
INSERT INTO fizzy.lookup (id, you_id, competitor_id, effectiveness) VALUES (188, 11, 8, 1);
INSERT INTO fizzy.lookup (id, you_id, competitor_id, effectiveness) VALUES (189, 11, 9, 1);
INSERT INTO fizzy.lookup (id, you_id, competitor_id, effectiveness) VALUES (190, 11, 10, 2);
INSERT INTO fizzy.lookup (id, you_id, competitor_id, effectiveness) VALUES (191, 11, 11, -1);
INSERT INTO fizzy.lookup (id, you_id, competitor_id, effectiveness) VALUES (192, 11, 12, -1);
INSERT INTO fizzy.lookup (id, you_id, competitor_id, effectiveness) VALUES (193, 11, 13, 1);
INSERT INTO fizzy.lookup (id, you_id, competitor_id, effectiveness) VALUES (194, 11, 14, 1);
INSERT INTO fizzy.lookup (id, you_id, competitor_id, effectiveness) VALUES (195, 11, 15, 1);
INSERT INTO fizzy.lookup (id, you_id, competitor_id, effectiveness) VALUES (196, 11, 16, -1);
INSERT INTO fizzy.lookup (id, you_id, competitor_id, effectiveness) VALUES (197, 11, 17, 1);
INSERT INTO fizzy.lookup (id, you_id, competitor_id, effectiveness) VALUES (198, 11, 18, 1);
INSERT INTO fizzy.lookup (id, you_id, competitor_id, effectiveness) VALUES (199, 12, 1, 1);
INSERT INTO fizzy.lookup (id, you_id, competitor_id, effectiveness) VALUES (200, 12, 2, 1);
INSERT INTO fizzy.lookup (id, you_id, competitor_id, effectiveness) VALUES (201, 12, 3, -1);
INSERT INTO fizzy.lookup (id, you_id, competitor_id, effectiveness) VALUES (202, 12, 4, -1);
INSERT INTO fizzy.lookup (id, you_id, competitor_id, effectiveness) VALUES (203, 12, 5, 2);
INSERT INTO fizzy.lookup (id, you_id, competitor_id, effectiveness) VALUES (204, 12, 6, 2);
INSERT INTO fizzy.lookup (id, you_id, competitor_id, effectiveness) VALUES (205, 12, 7, -1);
INSERT INTO fizzy.lookup (id, you_id, competitor_id, effectiveness) VALUES (206, 12, 8, 1);
INSERT INTO fizzy.lookup (id, you_id, competitor_id, effectiveness) VALUES (207, 12, 9, -1);
INSERT INTO fizzy.lookup (id, you_id, competitor_id, effectiveness) VALUES (208, 12, 10, -1);
INSERT INTO fizzy.lookup (id, you_id, competitor_id, effectiveness) VALUES (209, 12, 11, 2);
INSERT INTO fizzy.lookup (id, you_id, competitor_id, effectiveness) VALUES (210, 12, 12, -1);
INSERT INTO fizzy.lookup (id, you_id, competitor_id, effectiveness) VALUES (211, 12, 13, 1);
INSERT INTO fizzy.lookup (id, you_id, competitor_id, effectiveness) VALUES (212, 12, 14, 1);
INSERT INTO fizzy.lookup (id, you_id, competitor_id, effectiveness) VALUES (213, 12, 15, 1);
INSERT INTO fizzy.lookup (id, you_id, competitor_id, effectiveness) VALUES (214, 12, 16, -1);
INSERT INTO fizzy.lookup (id, you_id, competitor_id, effectiveness) VALUES (215, 12, 17, 1);
INSERT INTO fizzy.lookup (id, you_id, competitor_id, effectiveness) VALUES (216, 12, 18, 1);
INSERT INTO fizzy.lookup (id, you_id, competitor_id, effectiveness) VALUES (217, 13, 1, 1);
INSERT INTO fizzy.lookup (id, you_id, competitor_id, effectiveness) VALUES (218, 13, 2, 1);
INSERT INTO fizzy.lookup (id, you_id, competitor_id, effectiveness) VALUES (219, 13, 3, 2);
INSERT INTO fizzy.lookup (id, you_id, competitor_id, effectiveness) VALUES (220, 13, 4, 1);
INSERT INTO fizzy.lookup (id, you_id, competitor_id, effectiveness) VALUES (221, 13, 5, -1);
INSERT INTO fizzy.lookup (id, you_id, competitor_id, effectiveness) VALUES (222, 13, 6, 1);
INSERT INTO fizzy.lookup (id, you_id, competitor_id, effectiveness) VALUES (223, 13, 7, 1);
INSERT INTO fizzy.lookup (id, you_id, competitor_id, effectiveness) VALUES (224, 13, 8, 1);
INSERT INTO fizzy.lookup (id, you_id, competitor_id, effectiveness) VALUES (225, 13, 9, 1);
INSERT INTO fizzy.lookup (id, you_id, competitor_id, effectiveness) VALUES (226, 13, 10, 1);
INSERT INTO fizzy.lookup (id, you_id, competitor_id, effectiveness) VALUES (227, 13, 11, 2);
INSERT INTO fizzy.lookup (id, you_id, competitor_id, effectiveness) VALUES (228, 13, 12, -1);
INSERT INTO fizzy.lookup (id, you_id, competitor_id, effectiveness) VALUES (229, 13, 13, -1);
INSERT INTO fizzy.lookup (id, you_id, competitor_id, effectiveness) VALUES (230, 13, 14, 1);
INSERT INTO fizzy.lookup (id, you_id, competitor_id, effectiveness) VALUES (231, 13, 15, 1);
INSERT INTO fizzy.lookup (id, you_id, competitor_id, effectiveness) VALUES (232, 13, 16, -1);
INSERT INTO fizzy.lookup (id, you_id, competitor_id, effectiveness) VALUES (233, 13, 17, 1);
INSERT INTO fizzy.lookup (id, you_id, competitor_id, effectiveness) VALUES (234, 13, 18, 1);
INSERT INTO fizzy.lookup (id, you_id, competitor_id, effectiveness) VALUES (235, 14, 1, 1);
INSERT INTO fizzy.lookup (id, you_id, competitor_id, effectiveness) VALUES (236, 14, 2, 2);
INSERT INTO fizzy.lookup (id, you_id, competitor_id, effectiveness) VALUES (237, 14, 3, 1);
INSERT INTO fizzy.lookup (id, you_id, competitor_id, effectiveness) VALUES (238, 14, 4, 2);
INSERT INTO fizzy.lookup (id, you_id, competitor_id, effectiveness) VALUES (239, 14, 5, 1);
INSERT INTO fizzy.lookup (id, you_id, competitor_id, effectiveness) VALUES (240, 14, 6, 1);
INSERT INTO fizzy.lookup (id, you_id, competitor_id, effectiveness) VALUES (241, 14, 7, 1);
INSERT INTO fizzy.lookup (id, you_id, competitor_id, effectiveness) VALUES (242, 14, 8, 1);
INSERT INTO fizzy.lookup (id, you_id, competitor_id, effectiveness) VALUES (243, 14, 9, -1);
INSERT INTO fizzy.lookup (id, you_id, competitor_id, effectiveness) VALUES (244, 14, 10, 1);
INSERT INTO fizzy.lookup (id, you_id, competitor_id, effectiveness) VALUES (245, 14, 11, 1);
INSERT INTO fizzy.lookup (id, you_id, competitor_id, effectiveness) VALUES (246, 14, 12, 1);
INSERT INTO fizzy.lookup (id, you_id, competitor_id, effectiveness) VALUES (247, 14, 13, 1);
INSERT INTO fizzy.lookup (id, you_id, competitor_id, effectiveness) VALUES (248, 14, 14, -1);
INSERT INTO fizzy.lookup (id, you_id, competitor_id, effectiveness) VALUES (249, 14, 15, 1);
INSERT INTO fizzy.lookup (id, you_id, competitor_id, effectiveness) VALUES (250, 14, 16, 1);
INSERT INTO fizzy.lookup (id, you_id, competitor_id, effectiveness) VALUES (251, 14, 17, -1);
INSERT INTO fizzy.lookup (id, you_id, competitor_id, effectiveness) VALUES (252, 14, 18, 1);
INSERT INTO fizzy.lookup (id, you_id, competitor_id, effectiveness) VALUES (253, 15, 1, 1);
INSERT INTO fizzy.lookup (id, you_id, competitor_id, effectiveness) VALUES (254, 15, 2, 1);
INSERT INTO fizzy.lookup (id, you_id, competitor_id, effectiveness) VALUES (255, 15, 3, 2);
INSERT INTO fizzy.lookup (id, you_id, competitor_id, effectiveness) VALUES (256, 15, 4, 1);
INSERT INTO fizzy.lookup (id, you_id, competitor_id, effectiveness) VALUES (257, 15, 5, 2);
INSERT INTO fizzy.lookup (id, you_id, competitor_id, effectiveness) VALUES (258, 15, 6, 1);
INSERT INTO fizzy.lookup (id, you_id, competitor_id, effectiveness) VALUES (259, 15, 7, 1);
INSERT INTO fizzy.lookup (id, you_id, competitor_id, effectiveness) VALUES (260, 15, 8, 1);
INSERT INTO fizzy.lookup (id, you_id, competitor_id, effectiveness) VALUES (261, 15, 9, -1);
INSERT INTO fizzy.lookup (id, you_id, competitor_id, effectiveness) VALUES (262, 15, 10, -1);
INSERT INTO fizzy.lookup (id, you_id, competitor_id, effectiveness) VALUES (263, 15, 11, -1);
INSERT INTO fizzy.lookup (id, you_id, competitor_id, effectiveness) VALUES (264, 15, 12, 2);
INSERT INTO fizzy.lookup (id, you_id, competitor_id, effectiveness) VALUES (265, 15, 13, 1);
INSERT INTO fizzy.lookup (id, you_id, competitor_id, effectiveness) VALUES (266, 15, 14, 1);
INSERT INTO fizzy.lookup (id, you_id, competitor_id, effectiveness) VALUES (267, 15, 15, -1);
INSERT INTO fizzy.lookup (id, you_id, competitor_id, effectiveness) VALUES (268, 15, 16, 2);
INSERT INTO fizzy.lookup (id, you_id, competitor_id, effectiveness) VALUES (269, 15, 17, 1);
INSERT INTO fizzy.lookup (id, you_id, competitor_id, effectiveness) VALUES (270, 15, 18, 1);
INSERT INTO fizzy.lookup (id, you_id, competitor_id, effectiveness) VALUES (271, 16, 1, 1);
INSERT INTO fizzy.lookup (id, you_id, competitor_id, effectiveness) VALUES (272, 16, 2, 1);
INSERT INTO fizzy.lookup (id, you_id, competitor_id, effectiveness) VALUES (273, 16, 3, 1);
INSERT INTO fizzy.lookup (id, you_id, competitor_id, effectiveness) VALUES (274, 16, 4, 1);
INSERT INTO fizzy.lookup (id, you_id, competitor_id, effectiveness) VALUES (275, 16, 5, 1);
INSERT INTO fizzy.lookup (id, you_id, competitor_id, effectiveness) VALUES (276, 16, 6, 1);
INSERT INTO fizzy.lookup (id, you_id, competitor_id, effectiveness) VALUES (277, 16, 7, 1);
INSERT INTO fizzy.lookup (id, you_id, competitor_id, effectiveness) VALUES (278, 16, 8, 1);
INSERT INTO fizzy.lookup (id, you_id, competitor_id, effectiveness) VALUES (279, 16, 9, -1);
INSERT INTO fizzy.lookup (id, you_id, competitor_id, effectiveness) VALUES (280, 16, 10, 1);
INSERT INTO fizzy.lookup (id, you_id, competitor_id, effectiveness) VALUES (281, 16, 11, 1);
INSERT INTO fizzy.lookup (id, you_id, competitor_id, effectiveness) VALUES (282, 16, 12, 1);
INSERT INTO fizzy.lookup (id, you_id, competitor_id, effectiveness) VALUES (283, 16, 13, 1);
INSERT INTO fizzy.lookup (id, you_id, competitor_id, effectiveness) VALUES (284, 16, 14, 1);
INSERT INTO fizzy.lookup (id, you_id, competitor_id, effectiveness) VALUES (285, 16, 15, 1);
INSERT INTO fizzy.lookup (id, you_id, competitor_id, effectiveness) VALUES (286, 16, 16, 2);
INSERT INTO fizzy.lookup (id, you_id, competitor_id, effectiveness) VALUES (287, 16, 17, 1);
INSERT INTO fizzy.lookup (id, you_id, competitor_id, effectiveness) VALUES (288, 16, 18, -1);
INSERT INTO fizzy.lookup (id, you_id, competitor_id, effectiveness) VALUES (289, 17, 1, 1);
INSERT INTO fizzy.lookup (id, you_id, competitor_id, effectiveness) VALUES (290, 17, 2, -1);
INSERT INTO fizzy.lookup (id, you_id, competitor_id, effectiveness) VALUES (291, 17, 3, 1);
INSERT INTO fizzy.lookup (id, you_id, competitor_id, effectiveness) VALUES (292, 17, 4, 1);
INSERT INTO fizzy.lookup (id, you_id, competitor_id, effectiveness) VALUES (293, 17, 5, 1);
INSERT INTO fizzy.lookup (id, you_id, competitor_id, effectiveness) VALUES (294, 17, 6, 1);
INSERT INTO fizzy.lookup (id, you_id, competitor_id, effectiveness) VALUES (295, 17, 7, 1);
INSERT INTO fizzy.lookup (id, you_id, competitor_id, effectiveness) VALUES (296, 17, 8, 2);
INSERT INTO fizzy.lookup (id, you_id, competitor_id, effectiveness) VALUES (297, 17, 9, 1);
INSERT INTO fizzy.lookup (id, you_id, competitor_id, effectiveness) VALUES (298, 17, 10, 1);
INSERT INTO fizzy.lookup (id, you_id, competitor_id, effectiveness) VALUES (299, 17, 11, 1);
INSERT INTO fizzy.lookup (id, you_id, competitor_id, effectiveness) VALUES (300, 17, 12, 1);
INSERT INTO fizzy.lookup (id, you_id, competitor_id, effectiveness) VALUES (301, 17, 13, 1);
INSERT INTO fizzy.lookup (id, you_id, competitor_id, effectiveness) VALUES (302, 17, 14, 2);
INSERT INTO fizzy.lookup (id, you_id, competitor_id, effectiveness) VALUES (303, 17, 15, 1);
INSERT INTO fizzy.lookup (id, you_id, competitor_id, effectiveness) VALUES (304, 17, 16, 1);
INSERT INTO fizzy.lookup (id, you_id, competitor_id, effectiveness) VALUES (305, 17, 17, -1);
INSERT INTO fizzy.lookup (id, you_id, competitor_id, effectiveness) VALUES (306, 17, 18, -1);
INSERT INTO fizzy.lookup (id, you_id, competitor_id, effectiveness) VALUES (307, 18, 1, 1);
INSERT INTO fizzy.lookup (id, you_id, competitor_id, effectiveness) VALUES (308, 18, 2, 2);
INSERT INTO fizzy.lookup (id, you_id, competitor_id, effectiveness) VALUES (309, 18, 3, 1);
INSERT INTO fizzy.lookup (id, you_id, competitor_id, effectiveness) VALUES (310, 18, 4, -1);
INSERT INTO fizzy.lookup (id, you_id, competitor_id, effectiveness) VALUES (311, 18, 5, 1);
INSERT INTO fizzy.lookup (id, you_id, competitor_id, effectiveness) VALUES (312, 18, 6, 1);
INSERT INTO fizzy.lookup (id, you_id, competitor_id, effectiveness) VALUES (313, 18, 7, 1);
INSERT INTO fizzy.lookup (id, you_id, competitor_id, effectiveness) VALUES (314, 18, 8, 1);
INSERT INTO fizzy.lookup (id, you_id, competitor_id, effectiveness) VALUES (315, 18, 9, -1);
INSERT INTO fizzy.lookup (id, you_id, competitor_id, effectiveness) VALUES (316, 18, 10, -1);
INSERT INTO fizzy.lookup (id, you_id, competitor_id, effectiveness) VALUES (317, 18, 11, 1);
INSERT INTO fizzy.lookup (id, you_id, competitor_id, effectiveness) VALUES (318, 18, 12, 1);
INSERT INTO fizzy.lookup (id, you_id, competitor_id, effectiveness) VALUES (319, 18, 13, 1);
INSERT INTO fizzy.lookup (id, you_id, competitor_id, effectiveness) VALUES (320, 18, 14, 1);
INSERT INTO fizzy.lookup (id, you_id, competitor_id, effectiveness) VALUES (321, 18, 15, 1);
INSERT INTO fizzy.lookup (id, you_id, competitor_id, effectiveness) VALUES (322, 18, 16, 2);
INSERT INTO fizzy.lookup (id, you_id, competitor_id, effectiveness) VALUES (323, 18, 17, 2);
INSERT INTO fizzy.lookup (id, you_id, competitor_id, effectiveness) VALUES (324, 18, 18, 1);
