create table you
(
    id   int auto_increment
        primary key,
    type varchar(15) null
);

INSERT INTO fizzy.you (id, type) VALUES (1, 'Normal');
INSERT INTO fizzy.you (id, type) VALUES (2, 'Fighting');
INSERT INTO fizzy.you (id, type) VALUES (3, 'Flying');
INSERT INTO fizzy.you (id, type) VALUES (4, 'Poison');
INSERT INTO fizzy.you (id, type) VALUES (5, 'Ground');
INSERT INTO fizzy.you (id, type) VALUES (6, 'Rock');
INSERT INTO fizzy.you (id, type) VALUES (7, 'Bug');
INSERT INTO fizzy.you (id, type) VALUES (8, 'Ghost');
INSERT INTO fizzy.you (id, type) VALUES (9, 'Steel');
INSERT INTO fizzy.you (id, type) VALUES (10, 'Fire');
INSERT INTO fizzy.you (id, type) VALUES (11, 'Water');
INSERT INTO fizzy.you (id, type) VALUES (12, 'Grass');
INSERT INTO fizzy.you (id, type) VALUES (13, 'Electric');
INSERT INTO fizzy.you (id, type) VALUES (14, 'Psychic');
INSERT INTO fizzy.you (id, type) VALUES (15, 'Ice');
INSERT INTO fizzy.you (id, type) VALUES (16, 'Dragon');
INSERT INTO fizzy.you (id, type) VALUES (17, 'Dark');
INSERT INTO fizzy.you (id, type) VALUES (18, 'Fairy');
