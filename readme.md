# Pokémon Team Recommendation Tool

This repository contains a PHP script that serves as a Pokémon team recommendation tool. The script allows users to input their favourite Pokémon and estimate the team combination with the widest type coverage. The tool calculates individual versus effectiveness scores and generates unique teams based on the input.

## Dependencies

- PHP
- MySQL

## Files

The code in `form.php` represents a web form that allows users to input and select Pokémon names and types. It then processes the input data, performs calculations, and provides recommendations for Pokémon teams based on type coverage.

Here is a breakdown of what the code does:

1. Database Connection: It establishes a connection to a MySQL database named "fizzy" using the provided server credentials.

2. Function Definition: It defines a function called `pokemonform_block` that generates a block of HTML form inputs for a Pokémon's name and types. The function also performs input validation and displays error messages if necessary.

3. Form Input Processing: After the HTML form is submitted (`POST` request), the code processes the submitted data. It retrieves the Pokémon names and types from the form input fields, performs database queries to fetch additional data, and stores the input in the `$pokemon_input` array.

4. HTML Output: The code generates HTML markup for the web page, including the form, the list of selected Pokémon with their types, and recommendations for Pokémon teams based on type coverage.

5. Helper Functions: The code includes a few helper functions such as `test_input` for input validation and `unique_teams` for generating unique teams of Pokémon for analysis.

6. Pokémon Team Recommendation: The code performs calculations to determine the effectiveness scores of each Pokémon team. It evaluates the type coverage of each team, sorts the teams based on their scores, and displays the top-performing teams as recommendations.

7. Debugging Output: There is a section at the end (`<div id="debug">`) that can be uncommented to display debugging information such as the input data, the generated Pokémon teams, and any other relevant variables for troubleshooting purposes.

8. Database Connection Closure: The database connection is closed at the end of the script to free up resources.

Overall, `form.php` is a web form that allows users to input Pokémon names and types, and it provides recommendations for creating balanced Pokémon teams based on type coverage.

- `stylesheetflex.css`: CSS file for styling the HTML form.
- `javascript.js`: JavaScript file containing client-side functionality for the form.
- `README.md`: Instructions and information about the code.

## Notes

- The code connects to a MySQL database to retrieve type data for Pokémon.
- It provides functions to handle input validation and generate unique teams of Pokémon.
- The code calculates versus effectiveness scores for each Pokémon and team.
- The output displays the recommended teams along with their scores and type effectiveness.